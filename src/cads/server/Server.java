package cads.server;

import cads.helper.FreePortFinder;
import cads.helper.Logger;
import cads.network.CommunicatorServer;
import cads.server.robot.Robot;
import cads.server.services.EStopService;
import cads.server.services.GrabService;
import cads.server.services.HorizontalService;
import cads.server.services.VerticalService;

import org.cads.ev3.middleware.CaDSEV3RobotType;


public class Server
{

    public static void main(String[] args)
    {
        Logger.enableDebugMode(true);

        FreePortFinder portFinder = new FreePortFinder(1060);

        if (args.length == 2)
        {
        	Logger.log("Roboter");
            CommunicatorServer server = CommunicatorServer.getInstance();
            server.setBrokerIp(args[0]);
            server.setBrokerPort(Integer.parseInt(args[1]));

            Robot robot = new Robot();
            robot.init(CaDSEV3RobotType.SIMULATION);

            new GrabService(robot.getRobotUuid(),portFinder.getNextFreePort());
            new HorizontalService(robot.getRobotUuid(),portFinder.getNextFreePort());
            new VerticalService(robot.getRobotUuid(),portFinder.getNextFreePort());
            new EStopService(robot.getRobotUuid(),portFinder.getNextFreePort());
            
        }
        else
        {
            System.err.println(args.length + " Falsche Argumente");
        }
    }

}