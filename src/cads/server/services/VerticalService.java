package cads.server.services;

import java.util.UUID;

import cads.server.robot.queues.VerticalMovementQueue;

public class VerticalService extends AbstractService
{
    private VerticalMovementQueue q;

    public VerticalService(UUID robotName,int port)
    {
        super(robotName, port);
        q = VerticalMovementQueue.getInstance();
    }

    @Override
    public void executeCmd(String cmd)
    {
        try
        {
            int cmdInt = Integer.parseInt(cmd);
            q.service_method(cmdInt);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public String getServiceName()
    {
        return "moveVertical";
    }
    
	@Override
	public String getServiceProtocol()
	{
		return "tcp";
	}

	@Override
	public String getDescription()
	{
		return "Vertikale Bewegung des Greifarmes";
	}
	
	@Override
	public String getParameter()
	{
		return "range";
	}
}
