package cads.server.services;

import java.util.UUID;

import cads.server.robot.queues.HorizontalMovementQueue;

public class HorizontalService extends AbstractService
{
    private HorizontalMovementQueue q;

    public HorizontalService(UUID robotName,int port)
    {
        super(robotName, port);
        q = HorizontalMovementQueue.getInstance();
    }

    @Override
    public void executeCmd(String cmd)
    {
        try
        {
            int cmdInt = Integer.parseInt(cmd);
            q.service_method(cmdInt);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public String getServiceName()
    {
        return "moveHorizontal";
    }
    
	@Override
	public String getServiceProtocol()
	{
		return "tcp";
	}

	@Override
	public String getDescription()
	{
		return "Horizontale Bewegung des Greifarmes";
	}
	
	@Override
	public String getParameter()
	{
		return "range";
	}
}
