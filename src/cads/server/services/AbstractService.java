package cads.server.services;

import java.util.UUID;
import cads.network.CommunicatorServer;

public abstract class AbstractService
{

    UUID robotName;
    int servicePort;

    public AbstractService(UUID robotName, int port)
    {
        this.robotName = robotName;
        this.servicePort = port;
        CommunicatorServer comm = CommunicatorServer.getInstance();
        comm.registerService(this);
    }

    public UUID getRobotName()
    {
        return robotName;
    }

    public int getServicePort()
    {
        return servicePort;
    }

    abstract public void executeCmd(String cmd);

    abstract public String getServiceName();
    
    abstract public String getServiceProtocol();
    
    abstract public String getDescription();
    
    abstract public String getParameter();
}
