package cads.server.services;


import java.util.UUID;

import cads.helper.Logger;
import cads.server.robot.EmergencyDispatcher;

public class EStopService extends AbstractService
{

    private EmergencyDispatcher dispatcher;

	public EStopService(UUID robotName, int port)
    {
        super(robotName, port);
        dispatcher = EmergencyDispatcher.getInstance();
    }

    @Override
    public void executeCmd(String cmd)
    {
    	Logger.log(getServiceName());
    	dispatcher.dispatchEstopp();
    }

    @Override
    public String getServiceName()
    {
        return "emergencyStop";
    }

	@Override
	public String getServiceProtocol()
	{
		return "udp";
	}

	@Override
	public String getDescription()
	{
		return "Not-Halt f�r den Roboter";
	}

	@Override
	public String getParameter()
	{
		return "binary";
	}
}
