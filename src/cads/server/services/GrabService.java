package cads.server.services;


import java.util.UUID;

import cads.server.robot.queues.GrabMovementQueue;

public class GrabService extends AbstractService
{
    private GrabMovementQueue q;

    public GrabService(UUID robotName, int port)
    {
        super(robotName, port);
        q = GrabMovementQueue.getInstance();
    }

    @Override
    public void executeCmd(String cmd)
    {
        try
        {
            int cmdInt = Integer.parseInt(cmd);
            q.service_method(cmdInt);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public String getServiceName()
    {
        return "grab";
    }
    
	@Override
	public String getServiceProtocol()
	{
		return "tcp";
	}

	@Override
	public String getDescription()
	{
		return "Greifer am Roboter";
	}
	
	@Override
	public String getParameter()
	{
		return "binary";
	}
}
