package cads.server.robot;

import cads.server.robot.queues.GrabMovementQueue;
import cads.server.robot.queues.HorizontalMovementQueue;
import cads.server.robot.queues.VerticalMovementQueue;

import java.util.UUID;

import org.cads.ev3.middleware.CaDSEV3RobotHAL;
import org.cads.ev3.middleware.CaDSEV3RobotType;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotFeedBackListener;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotStatusListener;
import org.json.simple.JSONObject;


public class Robot implements ICaDSEV3RobotStatusListener, ICaDSEV3RobotFeedBackListener
{

    private enum StateHorizontal
    {
        LEFT, RIGHT, STOP
    }

    ;

    private enum StateVertical
    {
        UP, DOWN, STOP
    }

    ;

    private static CaDSEV3RobotHAL caller = null;
    private HorizontalLogic hl;
    private VerticalLogic vl;
    private GrabLogic gl;
    private UUID uuid;

    public Robot()
    {
        this.uuid = UUID.randomUUID();
    }

    public UUID getRobotUuid()
    {
        return uuid;
    }

    public void init(CaDSEV3RobotType type)
    {
        hl = new HorizontalLogic();
        vl = new VerticalLogic();
        gl = new GrabLogic();

        Thread hlThread = new Thread(hl);
        hlThread.start();

        Thread vlThread = new Thread(vl);
        vlThread.start();

        Thread glThread = new Thread(gl);
        glThread.start();

        caller = CaDSEV3RobotHAL.createInstance(type, this, this);

    }


    @Override
    public synchronized void onStatusMessage(JSONObject status)
    {
        if (status.get("type").toString().equals("GRIPPER_INFO"))
        {
            if (status.get("state").toString().equals("horizontal"))
            {

                hl.onStatusMessage(status);

            }
            else if (status.get("state").toString().equals("vertical"))
            {
                vl.onStatusMessage(status);

            }
            else if (status.get("state").toString().equals("gripper"))
            {
                gl.onStatusMessage(status);
            }
        }

    }

    @Override
    public void giveFeedbackByJSonTo(JSONObject arg0)
    {
        // TODO Auto-generated method stub

    }


    class HorizontalLogic implements ICaDSEV3RobotStatusListener, Runnable, IStoppable
    {

        private int lastValueHorizontal, targetValueHorizontal;
        private StateHorizontal movementHorizontal;

        HorizontalLogic()
        {
            EmergencyDispatcher.getInstance().addStoppable(this);
        }


        @Override
        public void run()
        {
            try
            {

                HorizontalMovementQueue horizontalQueue = HorizontalMovementQueue.getInstance();

                while (!Thread.currentThread().isInterrupted())
                {
                    targetValueHorizontal = horizontalQueue.dequeue();
                    if (lastValueHorizontal > targetValueHorizontal)
                    {
                        movementHorizontal = StateHorizontal.RIGHT;
                        caller.moveRight();
                    }
                    else if (lastValueHorizontal < targetValueHorizontal)
                    {
                        movementHorizontal = StateHorizontal.LEFT;
                        caller.moveLeft();

                    }
                    else
                    {
                        //Nichts der Roboter steht schon am Ziel
                    }

                }
            } catch (Exception e)
            {
                e.printStackTrace();
                System.exit(-1);
            }
            System.exit(0);

        }

        @Override
        public void onStatusMessage(JSONObject status)
        {

            lastValueHorizontal = Integer.parseInt(status.get("percent").toString());
            if (movementHorizontal == StateHorizontal.LEFT)
            {
                if (lastValueHorizontal >= targetValueHorizontal)
                {
                    movementHorizontal = StateHorizontal.STOP;
                    caller.stop_h();
                }
            }
            else if (movementHorizontal == StateHorizontal.RIGHT)
            {
                if (lastValueHorizontal <= targetValueHorizontal)
                {
                    movementHorizontal = StateHorizontal.STOP;
                    caller.stop_h();
                }
            }

        }

        @Override
        public void stop()
        {
        	HorizontalMovementQueue.getInstance().clear();
            caller.stop_h();
        }

    }


    class VerticalLogic implements ICaDSEV3RobotStatusListener, Runnable, IStoppable
    {
        private int lastValueVertical, targetValueVertical;
        private StateVertical movementVertical;

        VerticalLogic()
        {
            EmergencyDispatcher.getInstance().addStoppable(this);
        }

        @Override
        public void run()
        {
            try
            {

                VerticalMovementQueue verticalQueue = VerticalMovementQueue.getInstance();

                while (!Thread.currentThread().isInterrupted())
                {
                    targetValueVertical = verticalQueue.dequeue();
                    if (lastValueVertical < targetValueVertical)
                    {
                        movementVertical = StateVertical.UP;
                        caller.moveUp();
                    }
                    else if (lastValueVertical > targetValueVertical)
                    {
                        movementVertical = StateVertical.DOWN;
                        caller.moveDown();

                    }
                    else
                    {
                        //Nichts der Roboter steht schon am Ziel
                    }

                }
            } catch (Exception e)
            {
                e.printStackTrace();
                System.exit(-1);
            }
            System.exit(0);

        }

        @Override
        public void onStatusMessage(JSONObject status)
        {
            lastValueVertical = Integer.parseInt(status.get("percent").toString());
            if (movementVertical == StateVertical.UP)
            {
                if (lastValueVertical >= targetValueVertical)
                {
                    movementVertical = StateVertical.STOP;
                    caller.stop_v();
                }
            }
            else if (movementVertical == StateVertical.DOWN)
            {
                if (lastValueVertical <= targetValueVertical)
                {
                    movementVertical = StateVertical.STOP;
                    caller.stop_v();
                }
            }

        }

        @Override
        public void stop()
        {
        	VerticalMovementQueue.getInstance().clear();
            caller.stop_v();
        }

    }

    class GrabLogic implements ICaDSEV3RobotStatusListener, Runnable
    {
        private int lastValueGrab = 1, targetValueGrab = 1;

        @Override
        public void run()
        {
            try
            {

                GrabMovementQueue grabQueue = GrabMovementQueue.getInstance();

                while (!Thread.currentThread().isInterrupted())
                {
                    targetValueGrab = grabQueue.dequeue();
                    if (lastValueGrab < targetValueGrab)
                    {
                        caller.doOpen();
                    }
                    else if (lastValueGrab > targetValueGrab)
                    {
                        caller.doClose();
                    }
                    else
                    {
                        //Nichts der Roboter steht schon am Ziel
                    }

                }
            } catch (Exception e)
            {
                e.printStackTrace();
                System.exit(-1);
            }
            System.exit(0);

        }

        @Override
        public void onStatusMessage(JSONObject status)
        {
            if (status.get("value").toString().matches("open"))
            {
                lastValueGrab = 1;
            }
            else if (status.get("value").toString().matches("closed"))
            {
                lastValueGrab = 0;
            }
            else
            {
                //ERROR
            }


        }

    }

}



