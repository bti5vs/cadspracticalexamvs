package cads.server.robot;

import java.util.HashSet;
import java.util.Set;

public class EmergencyDispatcher
{
    private static EmergencyDispatcher eDispatcher;
    Set<IStoppable> map;

    private EmergencyDispatcher()
    {
        map = new HashSet<IStoppable>();
    }

    // Singleton getter
    public static synchronized EmergencyDispatcher getInstance()
    {
        if (eDispatcher == null)
        {
            eDispatcher = new EmergencyDispatcher();
        }
        return eDispatcher;
    }

    public void addStoppable(IStoppable ref)
    {
        map.add(ref);
    }

    public void dispatchEstopp()
    {
        for (IStoppable stoppable : map)
        {
            stoppable.stop();
        }
    }

}
