package cads.server.robot.queues;

import cads.interfaces.IService;

public class HorizontalMovementQueue extends AbstractBlockingQueue implements IService
{

    // Singleton instance
    private static HorizontalMovementQueue instance;

    private HorizontalMovementQueue()
    {
        // Initialize empty queue on singleton call
        setup();
    }

    // Singleton getter
    public static synchronized HorizontalMovementQueue getInstance()
    {
        if (HorizontalMovementQueue.instance == null)
        {
            HorizontalMovementQueue.instance = new HorizontalMovementQueue();
        }
        return HorizontalMovementQueue.instance;
    }

	@Override
	public String getName()
	{
		return "moveHorizontal";
	}

	@Override
	public void service_method(int targetValue)
	{
		enqueue(targetValue);
		
	}

}
