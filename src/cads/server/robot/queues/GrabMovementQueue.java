package cads.server.robot.queues;

import cads.interfaces.IService;

public class GrabMovementQueue extends AbstractBlockingQueue implements IService
{

    // Singleton instance
    private static GrabMovementQueue instance;

    private GrabMovementQueue()
    {
        // Initialize empty queue on singleton call
        setup();
    }

    // Singleton getter
    public static synchronized GrabMovementQueue getInstance()
    {
        if (GrabMovementQueue.instance == null)
        {
            GrabMovementQueue.instance = new GrabMovementQueue();
        }
        return GrabMovementQueue.instance;
    }

	@Override
	public void service_method(int targetValue)
	{
		enqueue(targetValue);
		
	}

	@Override
	public String getName()
	{
		return "grab";
	}
    

}
