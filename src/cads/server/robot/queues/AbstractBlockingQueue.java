package cads.server.robot.queues;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public abstract class AbstractBlockingQueue
{

    private BlockingQueue<Integer> queue;

    public void setup()
    {
        if (queue == null)
        {
            queue = new ArrayBlockingQueue<Integer>(10);
        }
    }

    public int dequeue()
    {

        setup();
        int result = -1;
        try
        {
            result = queue.take().intValue();

        } catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;

    }

    protected void enqueue(int input)
    {
        setup();
        try
        {
            queue.put(input);

        } catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

	public void clear()
	{
		queue.clear();		
	}

}
