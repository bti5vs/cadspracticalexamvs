package cads.server.robot.queues;

import cads.interfaces.IService;

public class VerticalMovementQueue extends AbstractBlockingQueue implements IService
{

    // Singleton instance
    private static VerticalMovementQueue instance;

    private VerticalMovementQueue()
    {
        // Initialize empty queue on singleton call
        setup();
    }

    // Singleton getter
    public static synchronized VerticalMovementQueue getInstance()
    {
        if (VerticalMovementQueue.instance == null)
        {
            VerticalMovementQueue.instance = new VerticalMovementQueue();
        }
        return VerticalMovementQueue.instance;
    }


	@Override
	public String getName()
	{
		return "moveVertical";
	}

	@Override
	public void service_method(int targetValue)
	{
		enqueue(targetValue);
		
	}

}
