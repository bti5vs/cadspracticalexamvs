package cads.server.robot;

public interface IStoppable
{
    public void stop();
}
