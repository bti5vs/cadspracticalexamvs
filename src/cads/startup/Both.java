package cads.startup;

import cads.client.Client;
import cads.helper.Logger;
import cads.server.Server;

public class Both
{
    public static void main(String[] args)
    {
        Logger.enableDebugMode(true);

        String[] argumentsServer = {"127.0.0.1", "8080"};
        String[] argumentsClient = {"127.0.0.1", "8888"};

        // Start Server
        Server.main(argumentsServer);

        // Start Client
        Client.main(argumentsClient);
    }

}
