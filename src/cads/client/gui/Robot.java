package cads.client.gui;

import java.util.UUID;

public class Robot
{
	private final UUID uuid;
	private final String name;

	public Robot(UUID uuid, String name)
	{
		this.uuid = uuid;
		this.name = name;
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getName()
	{
		return name;
	}

	@Override
	public int hashCode()
	{
		return uuid.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (obj == null)
		{
			return false;
		}

		if (getClass() != obj.getClass())
		{
			return false;
		}

		Robot other = (Robot) obj;
		if (uuid == null)
		{
			if (other.uuid != null)
			{
				return false;
			}
		} else if (!uuid.equals(other.uuid))
		{
			return false;
		}
		return true;
	}
}