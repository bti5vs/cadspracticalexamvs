package cads.client.gui;

import java.util.Set;
import java.util.UUID;

import org.cads.ev3.gui.ICaDSRobotGUIUpdater;
import org.cads.ev3.gui.swing.CaDSRobotGUISwing;
import org.cads.ev3.rmi.consumer.ICaDSRMIConsumer;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveGripper;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveHorizontal;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveVertical;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIUltraSonic;
import org.json.simple.JSONObject;

import cads.helper.Logger;
import cads.network.CommunicatorClient;

public class Gui extends Thread implements ICaDSRMIConsumer, IIDLCaDSEV3RMIMoveGripper, IIDLCaDSEV3RMIMoveVertical,
		IIDLCaDSEV3RMIMoveHorizontal, IIDLCaDSEV3RMIUltraSonic
{
	private Robot activeRobot;
	private CommunicatorClient comm;
	private String guiServerIP;
	private int guiServerPort;
	private JSONObject robots;

	public Gui(String guiServerIP, int guiServerPort)
	{
		this.guiServerIP = guiServerIP;
		this.guiServerPort = guiServerPort;
		comm = new CommunicatorClient(this.guiServerIP, this.guiServerPort);
	}

	@Override
	public void run()
	{
		CaDSRobotGUISwing gui = new CaDSRobotGUISwing(this, this, this, this, this);

		robots = comm.sendGetServices();

		Set<String> keys = robots.keySet();

		for (String name : keys)
		{
			gui.addService(name);
		}
	}

	@Override
	public int isUltraSonicOccupied() throws Exception
	{
		Logger.log("isUltraSonicOccupied");
		return 0;
	}

	@Override
	public int getCurrentHorizontalPercent() throws Exception
	{
		Logger.log("getCurrentHorizontalPercent");
		return 0;
	}

	@Override
	public int moveHorizontalToPercent(int arg0, int arg1) throws Exception
	{
		final String serviceName = "moveHorizontal";
		JSONObject services = getServices(activeRobot);
		JSONObject service = getService(services, serviceName);
		String serviceProtocol = (String) service.get("protocol");
		executeCmd(serviceProtocol, serviceName, arg1);
		return 0;
	}

	@Override
	public int getCurrentVerticalPercent() throws Exception
	{
		Logger.log("getCurrentVerticalPercent");
		return 0;
	}

	@Override
	public int moveVerticalToPercent(int arg0, int arg1) throws Exception
	{
		final String serviceName = "moveVertical";
		JSONObject services = getServices(activeRobot);
		JSONObject service = getService(services, serviceName);
		String serviceProtocol = (String) service.get("protocol");
		executeCmd(serviceProtocol, serviceName, arg1);
		return 0;
	}

	@Override
	public int stop(int arg0) throws Exception
	{
		Logger.log("stop arg0:" + arg0);
		return 0;
	}

	@Override
	public int closeGripper(int arg0) throws Exception
	{
//		final String serviceName = "grab";
//		JSONObject services = getServices(activeRobot);
//		JSONObject service = getService(services, serviceName);
//		String serviceProtocol = (String) service.get("protocol");
//		executeCmd(serviceProtocol, serviceName, 0);
		
		final String serviceName = "emergencyStop";
		JSONObject services = getServices(activeRobot);
		JSONObject service = getService(services, serviceName);
		String serviceProtocol = (String) service.get("protocol");
		executeCmd(serviceProtocol, serviceName, 0);
		return 0;
	}

	@Override
	public int isGripperClosed() throws Exception
	{
		Logger.log("isGripperClosed");
		return 0;
	}

	@Override
	public int openGripper(int arg0) throws Exception
	{
//		final String serviceName = "grab";
//		JSONObject services = getServices(activeRobot);
//		JSONObject service = getService(services, serviceName);
//		String serviceProtocol = (String) service.get("protocol");
//		executeCmd(serviceProtocol, serviceName, 1);
		
		final String serviceName = "emergencyStop";
		JSONObject services = getServices(activeRobot);
		JSONObject service = getService(services, serviceName);
		String serviceProtocol = (String) service.get("protocol");
		executeCmd(serviceProtocol, serviceName, 0);
		return 0;
	}

	@Override
	public void register(ICaDSRobotGUIUpdater arg0)
	{
		Logger.log("register arg0:" + arg0);

	}

	@Override
	public void update(String arg0)
	{
		Logger.log("update arg0:" + arg0);
		UUID id = UUID.fromString(arg0);
		activeRobot = new Robot(id, "");

	}

	private JSONObject getServices(Robot robot)
	{
		return (JSONObject) robots.get(robot.getUuid().toString());
	}

	private JSONObject getService(JSONObject services, String serviceName)
	{
		return (JSONObject) services.get(serviceName);
	}

	private void executeCmd(String serviceProtocol, String serviceName, int arg1)
	{
		if (serviceProtocol != null)
		{
			Logger.log("GUI issued command " + serviceName + "(value:" + arg1 + ")");
			if (serviceProtocol.matches("udp"))
			{
				comm.sendMessageUDP(activeRobot.getUuid(), serviceName, arg1);
			} else
			{
				comm.sendMessage(activeRobot.getUuid(), serviceName, arg1);
			}
		} else
		{
			Logger.log("Service vom Ger�t nicht unterst�tzt");
		}

	}

}
