package cads.client;

import cads.client.gui.Gui;
import cads.helper.Logger;

public class Client
{

	public static void main(String[] args)
	{

		Logger.enableDebugMode(true);

		if (args.length == 2)
		{
			String guiServerIp = args[0];
			int guiServerPort = Integer.parseInt(args[1]);

			Gui g = new Gui(guiServerIp, guiServerPort);
			g.start();
		} else
		{
			System.err.println("Falsche Parameter");
		}

	}

}
