package cads.helper;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Source:
 * https://stackoverflow.com/questions/2675362/how-to-find-an-available-port
 */
public class FreePortFinder
{
    private int basePort = 1025;

    public FreePortFinder(int basePort)
    {
        this.basePort = basePort;
    }

    public synchronized int getNextFreePort()
    {
    	basePort++;
        while (!isLocalPortFree(basePort))
        {
            basePort++;
        }
        return basePort;
    }

    private boolean isLocalPortFree(int port)
    {
        try
        {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e)
        {
            return false;
        }
    }
}
