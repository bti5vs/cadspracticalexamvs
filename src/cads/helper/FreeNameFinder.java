package cads.helper;

/**
 * Created by preya on 07.06.17.
 */
public class FreeNameFinder
{
    private static FreeNameFinder instance;
    private int counter;

    public static synchronized FreeNameFinder getInstance()
    {
        if (instance == null)
        {
            instance = new FreeNameFinder();
        }

        return instance;
    }

    public synchronized String getNextName()
    {
        return "robot" + counter++;
    }

}
