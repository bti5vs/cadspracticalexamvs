package cads.helper;

public class Logger
{

    private static boolean debuggingEnabled;

    public static void log(String msg)
    {
        if (debuggingEnabled)
        {
            System.out.println("Thread " + Thread.currentThread().getId() + ": " + msg);
        }
    }

    public static void enableDebugMode(boolean isEnabled)
    {
        debuggingEnabled = isEnabled;
    }

}
