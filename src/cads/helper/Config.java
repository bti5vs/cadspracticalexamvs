package cads.helper;

public class Config
{
    private static Config instance;
    private int servicesBasePort;
    private String brokerIp;
    private int brokerRegisterPort;
    private int brokerCommandPort;

    public int getBrokerCommandPort()
    {
        return brokerCommandPort;
    }

    public void setBrokerCommandPort(int brokerCommandPort)
    {
        this.brokerCommandPort = brokerCommandPort;
    }

    public static synchronized Config getInstance()
    {
        if (instance == null)
        {
            instance = new Config();
        }

        return instance;

    }

    public Config(int servicesBasePort, String brokerIp, int brokerRegisterPort)
    {
        this.servicesBasePort = servicesBasePort;
        this.brokerIp = brokerIp;
        this.brokerRegisterPort = brokerRegisterPort;
    }

    public Config()
    {
    }

    public int getServicesBasePort()
    {
        return servicesBasePort;
    }

    public String getBrokerIp()
    {
        return brokerIp;
    }

    public int getBrokerRegisterPort()
    {
        return brokerRegisterPort;
    }

    public void setServicesBasePort(int servicesBasePort)
    {
        this.servicesBasePort = servicesBasePort;
    }

    public void setBrokerIp(String brokerIp)
    {
        this.brokerIp = brokerIp;
    }

    public void setBrokerRegisterPort(int brokerRegisterPort)
    {
        this.brokerRegisterPort = brokerRegisterPort;
    }
}
