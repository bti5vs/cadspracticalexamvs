package cads.GuiServer;

import cads.helper.Config;
import cads.helper.Logger;

public class GuiServer
{

	public static void main(String[] args)
	{
		if (args.length == 2)
		{
			Logger.enableDebugMode(true);
			Config config = Config.getInstance();
			config.setBrokerIp(args[0]);
			config.setBrokerCommandPort(Integer.parseInt(args[1]));

			CommunicatorGuiServer guiServer = CommunicatorGuiServer.getInstance();

		} else
		{
			System.err.println("Falsche Parameter");
		}

	}

}
