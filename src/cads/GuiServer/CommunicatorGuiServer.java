package cads.GuiServer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import cads.helper.Config;
import cads.helper.Logger;
import cads.network.SocketFactory;
import cads.network.UDPFactory;
import lejos.utility.Delay;

public class CommunicatorGuiServer
{
	private static CommunicatorGuiServer comm;
	private int brokerPort;
	private String brokerIP;
	private int guiServerPort;
	private List<Socket> socketList;

	public static synchronized CommunicatorGuiServer getInstance()
	{
		if (comm == null)
		{
			Config config = Config.getInstance();
			comm = new CommunicatorGuiServer(config.getBrokerCommandPort(), config.getBrokerIp(), 8000);
		}
		return comm;
	}

	private CommunicatorGuiServer(int brokerPort, String brokerIP, int guiServerPort)
	{
		this.brokerPort = brokerPort;
		this.brokerIP = brokerIP;
		this.guiServerPort = guiServerPort;

		//socketList = Collections.synchronizedList(new ArrayList<Socket>());
		socketList = new CopyOnWriteArrayList<Socket>();
		new Thread(new ServiceListener()).start();
		new Thread(new Worker()).start();

		new Thread(new UDPWorker()).start();
	}

	///// From GuiServer to Broker
	public void sendMessageToBroker(String msg)
	{
		Socket s = SocketFactory.generateSocket(brokerIP, brokerPort);
		SocketFactory.sendMsg(s, msg);
		SocketFactory.closeSocket(s);
	}

	public String sendGetServicestoBroker()
	{
		Socket s = SocketFactory.generateSocket(brokerIP, brokerPort);

		String response = "";

		// Create message
		JSONObject msg = new JSONObject();
		msg.put("type", "getservices");

		// Send message to broker
		SocketFactory.sendMsg(s, msg.toString());

		// Read response from broker
		response = SocketFactory.readMsg(s);

		// Close writers and socket
		SocketFactory.closeSocket(s);

		return response;

	}

	///// From Gui to Gui Server
	/**
	 * Inner class handling all incoming TCP requests
	 * 
	 */
	private class ServiceListener implements Runnable
	{
		public ServiceListener()
		{

		}

		@Override
		public void run()
		{
			ServerSocket welcomeSocket = SocketFactory.generateServerSocket(guiServerPort);

			Logger.log("Gui Server listening on port " + welcomeSocket.getLocalPort());

			while (true)
			{
				// Blocking here until an incoming connection is received
				Socket clientSocket = SocketFactory.waitForSocket(welcomeSocket);

				socketList.add(clientSocket);
				Logger.log("Received a new connection. Socket Queued.");
			}
		}
	}

	/**
	 * Inner worker class handling all incoming TCP requests in a separate
	 * thread.
	 */
	private class Worker implements Runnable
	{
		public Worker()
		{
		}

		@Override
		public void run()
		{
			while (true)
			{
				for (Socket socket : socketList)
				{
					String command = SocketFactory.tryReadMsg(socket);
					if (command != null)
					{
						try
						{
							// Parse JSON
							JSONParser parser = new JSONParser();
							JSONObject jsonObject;

							jsonObject = (JSONObject) parser.parse(command);

							Logger.log("Command received:" + command);
							delegateCommand(jsonObject, socket);
							Logger.log("Command executed:" + command);
						} catch (ParseException e)
						{
							e.printStackTrace();
						}
					}
					Delay.msDelay(50);
				}
			}
		}

		private void delegateCommand(JSONObject command, Socket sender)
		{
			switch ((String) command.get("type"))
			{
			case "movement":
				movementCommand(command);
				break;

			case "getservices":
				String services = sendGetServicestoBroker();
				SocketFactory.sendMsg(sender, services);
				break;
			default:
				Logger.log("Unknown Command Typ");
				break;
			}
		}

		private void movementCommand(JSONObject command)
		{
			sendMessageToBroker(command.toJSONString());
		}

	}

	/**
	 * Inner worker class handling all UDP requests in a separate thread.
	 */
	private class UDPWorker implements Runnable
	{
		private DatagramSocket socket;

		public UDPWorker()
		{
			socket = UDPFactory.generateSocket(guiServerPort);
		}

		@Override
		public void run()
		{
			while (true)
			{
				Logger.log("UPD wartet");
				DatagramPacket packet = UDPFactory.waitForUDPPacket(socket, 1024);
				UDPFactory.answerUDPPacket(packet.getAddress(), packet.getPort(), packet.getData());
				Logger.log("UPD zurueck an Client");

				Logger.log("UPD an Broker");
				UDPFactory.sendUDPPacket(brokerIP, brokerPort, packet.getData());
				Logger.log("UPD an Broker gesendet");
			}
		}
	}

}
