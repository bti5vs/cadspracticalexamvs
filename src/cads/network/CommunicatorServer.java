package cads.network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import cads.helper.Logger;
import cads.server.services.AbstractService;

/**
 * This is a communication component for a server application. It creates a new
 * Listener thread and waits for incoming connections. Every successful
 * connection is being handed off to a new Worker thread.
 *
 * @author Moritz Stueckler
 */
public class CommunicatorServer
{

	private ExecutorService executor;
	private static CommunicatorServer comm;
	private String brokerIp;
	private int brokerPort;

	public static CommunicatorServer getInstance()
	{
		if (comm == null)
		{
			comm = new CommunicatorServer();
		}
		return comm;
	}

	public void setBrokerIp(String brokerIp)
	{
		this.brokerIp = brokerIp;
	}

	public void setBrokerPort(int brokerPort)
	{
		this.brokerPort = brokerPort;
	}

	public void registerService(AbstractService service)
	{
		if (service.getServiceProtocol().matches("udp"))
		{
			new Thread(new UDPWorker(service)).start();
		} else
		{
			new Thread(new ServiceListener(service)).start();
		}
		registerServiceAtBroker(service.getRobotName(), service.getServiceName(), service.getServicePort(),
				service.getServiceProtocol(), service.getDescription(), service.getParameter());
	}

	private void registerServiceAtBroker(UUID robotName, String serviceName, int port, String protocol,
			String discription, String parameter)
	{
		Socket s = SocketFactory.generateSocket(brokerIp, brokerPort);

		JSONObject msg = new JSONObject();
		msg.put("type", "register");
		msg.put("name", robotName.toString());
		msg.put("ip", s.getLocalAddress().toString().substring(1));// s.getLocalAddress().toString()
																	// ==
																	// "/127.0.0.1"
																	// Testing
																	// needed
																	// for other
																	// ips

		JSONArray services = new JSONArray();
		JSONObject service = new JSONObject();
		service.put("name", serviceName);
		service.put("port", port);
		service.put("protocol", protocol);
		service.put("description", discription);
		service.put("parameter", parameter);

		services.add(service);
		msg.put("services", services);
		Logger.log(msg.toJSONString());

		// Send message
		SocketFactory.sendMsg(s, msg.toJSONString());
		SocketFactory.closeSocket(s);
	}

	/**
	 * Inner class handling all incoming requests on his ServicePort
	 *
	 * @author Alexander Buhk
	 */
	private class ServiceListener implements Runnable
	{
		private AbstractService skeleton;

		public ServiceListener(AbstractService service)
		{
			this.skeleton = service;
			executor = Executors.newSingleThreadExecutor();

		}

		@Override
		public void run()
		{
			ServerSocket welcomeSocket = SocketFactory.generateServerSocket(skeleton.getServicePort());

			Logger.log("Server listening on port " + welcomeSocket.getLocalPort());

			while (true)
			{
				// Blocking here until an incoming connection is received
				Socket clientSocket = SocketFactory.waitForSocket(welcomeSocket);

				executor.execute(new Worker(clientSocket, skeleton));
				Logger.log("Service received a new connection. Worker thread from Threadpool queued");
			}
		}
	}

	/**
	 * Inner worker class handling all incoming network requests in a separate
	 * thread.
	 *
	 * @author Moritz Stueckler
	 */
	private class Worker implements Runnable
	{
		private Socket socket;
		private AbstractService skeleton;

		public Worker(Socket socket, AbstractService skeleton)
		{
			this.socket = socket;
			this.skeleton = skeleton;
		}

		@Override
		public void run()
		{
			String command = SocketFactory.tryReadMsg(socket);
			if (command != null)
			{
				Logger.log("Command received and added to queue: " + command);
				skeleton.executeCmd(command);
			} else
			{
				executor.execute(new Worker(socket, skeleton));
			}
		}

	}

	/**
	 * Inner worker class handling all UDP requests in a separate thread.
	 */
	private class UDPWorker implements Runnable
	{
		private DatagramSocket socket;
		private int port;
		private AbstractService service;

		public UDPWorker(AbstractService service)
		{
			this.service = service;
			this.port = service.getServicePort();
			socket = UDPFactory.generateSocket(port);
		}

		@Override
		public void run()
		{
			while (true)
			{
				try
				{
					Logger.log("UPD wartet");
					DatagramPacket packet = UDPFactory.waitForUDPPacket(socket, port);
					UDPFactory.answerUDPPacket(packet.getAddress(), packet.getPort(), packet.getData());
					Logger.log("UPD zurueck");

					String message = new String (packet.getData());
					message = message.trim();
					


					Logger.log("Service received UDP message: " + message);
					
					service.executeCmd(message);
					

				} catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
