package cads.network;

import java.net.Socket;
import java.util.UUID;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CommunicatorClient
{
	private Socket socket;
	private String guiServerIP;
	private int guiServerPort;

	public CommunicatorClient(String guiServerIP, int guiServerPort)
	{
		this.guiServerIP = guiServerIP;
		this.guiServerPort = guiServerPort;
		socket = SocketFactory.generateSocket(this.guiServerIP, this.guiServerPort);
	}

	public void sendMessage(String msg)
	{
		SocketFactory.sendMsg(socket, msg);
	}

	public String readMessage()
	{
		return SocketFactory.readMsg(socket);
	}

	public JSONObject sendGetServices()
	{

		String response = "";

		// Create message
		JSONObject msg = new JSONObject();
		msg.put("type", "getservices");

		// Send message to broker
		sendMessage(msg.toString());

		// Read response from broker
		response = readMessage();

		JSONParser parser = new JSONParser();

		Object obj = null;
		try
		{
			obj = parser.parse(response);

		} catch (ParseException e)
		{
			e.printStackTrace();
		}

		JSONObject robotNames = (JSONObject) obj;

		return robotNames;
	}

	public void sendMessage(UUID name, String method, int targetValue)
	{
		JSONObject msg = generateMsg(name, method, targetValue);
		sendMessage(msg.toString());
	}

	public void sendMessageUDP(UUID name, String method, int targetValue)
	{
		JSONObject msg = generateMsg(name, method, targetValue);
		UDPFactory.sendUDPPacket(guiServerIP, guiServerPort, msg.toString());
	}

	private JSONObject generateMsg(UUID name, String method, int targetValue)
	{
		JSONObject msg = new JSONObject();
		msg.put("type", "movement");
		msg.put("name", name.toString());
		msg.put("service", method);
		msg.put("value", targetValue + "");
		return msg;
	}
}
