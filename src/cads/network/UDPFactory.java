package cads.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import cads.helper.Logger;

public class UDPFactory
{
	public static DatagramSocket generateSocket(int port)
	{
		DatagramSocket s = null;
		try
		{
			s = new DatagramSocket(port);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}

	public static void closeSocket(DatagramSocket s)
	{
		s.close();
	}

	public static void sendUDPPacket(String brokerIP, int brokerPort, byte[] data)
	{
		try
		{
			DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName(brokerIP), brokerPort);
			sendUDPPacket(packet);
		} catch (UnknownHostException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void sendUDPPacket(String ip, int port, String data)
	{
		byte[] dataToSend = data.getBytes();
		sendUDPPacket(ip, port, dataToSend);
	}

	private static void sendUDPPacket(DatagramPacket packet)
	{
		try
		{
			DatagramSocket toSocket = new DatagramSocket();

			Thread udpListener = new Thread(new UDPListener(toSocket, packet.getLength(), packet));
			udpListener.start();

			for(int i=0;i<10||!udpListener.isAlive();i++)
			{
				toSocket.send(packet);
			}
			udpListener.stop();
			closeSocket(toSocket);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void answerUDPPacket(InetAddress address, int port, byte[] data)
	{
		try
		{
			DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
			DatagramSocket toSocket = new DatagramSocket();

			toSocket.send(packet);

			closeSocket(toSocket);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Inner class handling incomming UDP traffic
	 *
	 * @author Alexander Buhk
	 */
	private static class UDPListener implements Runnable
	{
		private DatagramSocket socket;
		private int size;
		private String compareString;

		public UDPListener(DatagramSocket socket, int size, DatagramPacket packet)
		{
			this.socket = socket;
			this.size = size;
			this.compareString = new String(packet.getData());
		}

		@Override
		public void run()
		{
			DatagramPacket received = null;
			while (received == null)
			{
				received = waitForUDPPacket(socket, size);
			}
			
			Logger.log("UDP Antwort erhalten");
		}
	}

	public static DatagramPacket waitForUDPPacket(DatagramSocket socket, int size)
	{
		try
		{
			DatagramPacket packet = new DatagramPacket(new byte[size], size);
			socket.receive(packet);
			return packet;
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
