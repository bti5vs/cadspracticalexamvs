package cads.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketFactory
{
	public static ServerSocket generateServerSocket(int port)
	{
		ServerSocket s = null;
		try
		{
			s = new ServerSocket(port);
		} catch (UnknownHostException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}

	public static Socket waitForSocket(ServerSocket s)
	{
		Socket result = null;
		try
		{
			result = s.accept();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public static Socket generateSocket(String ip, int port)
	{
		Socket s = null;
		try
		{
			s = new Socket(InetAddress.getByName(ip), port);
		} catch (UnknownHostException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}

	public static void closeSocket(Socket s)
	{
		try
		{
			s.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void sendMsg(Socket s, String msg)
	{
		try
		{
			PrintWriter outWriter = new PrintWriter(s.getOutputStream(), true);

			// Send message
			outWriter.println(msg);

		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Blocking
	 * <p>
	 * Liesst eine Zeile ein. Gibt einen einen Fehler wird Null zur�ck
	 * gegeben.
	 *
	 * @param s
	 *            der Socket auf dem gelesen werden soll.
	 * @return die eingelesende Nachricht oder Null bei einem Fehler.
	 */
	public static String readMsg(Socket s)
	{
		String result = "";
		try
		{
			BufferedReader incoming = new BufferedReader(new InputStreamReader(s.getInputStream()));

			// Blocking read
			result = incoming.readLine();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Versucht zu lesen. Wenn keine Daten da sind wird Null zurück gegeben.
	 *
	 * @param s
	 *            der Socket auf dem gelesen werden soll.
	 * @return die eingelesende Nachricht oder Null bei keinen Daten.
	 */
	public static String tryReadMsg(Socket s)
	{
		String result = null;
		try
		{
			if (s.getInputStream().available() > 0)
			{
				result = readMsg(s);
			}
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
}
